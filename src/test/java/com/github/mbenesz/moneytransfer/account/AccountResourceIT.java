package com.github.mbenesz.moneytransfer.account;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.mbenesz.moneytransfer.db.Database;
import com.github.mbenesz.moneytransfer.transfer.Transfer;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.eclipse.jetty.http.HttpStatus.Code.NOT_FOUND;
import static org.eclipse.jetty.http.HttpStatus.Code.NO_CONTENT;
import static org.eclipse.jetty.http.HttpStatus.Code.OK;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
class AccountResourceIT {
	private Server server;
	private Database database;
	private OkHttpClient client = new OkHttpClient();
	private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	private ObjectMapper mapper = new ObjectMapper();

	@BeforeAll
	void before() throws Exception {
		database = Database.getInstance();

		server = new Server(8081);

		ServletContextHandler context = new ServletContextHandler();
		context.setContextPath("/");
		server.setHandler(context);

		ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/api/*");
		servletHolder.setInitOrder(0);
		servletHolder.setInitParameter(
				"jersey.config.server.provider.packages",
				"com.github.mbenesz.moneytransfer"
		);
		server.setHandler(context);
		server.start();
	}

	@AfterAll
	void after() {
		try {
			server.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterEach
	void afterEach() {
		database.clear();
	}

	@DisplayName("Should return account for provided uid")
	@Test
	void shouldReturnAccountForProvidedUid() throws IOException {
		// Given
		database.addAccount(5L);
		database.deposit(5L, 400L);

		// When
		Request request = new Request.Builder()
				.url("http://localhost:8081/api/account/5")
				.get()
				.build();
		Response response = client.newCall(request).execute();

		// Then
		String jsonResponse = response.body().string();
		Account account = mapper.readValue(jsonResponse, Account.class);
		assertThat(account).isEqualToComparingOnlyGivenFields(new Account(5L, 400L), "uid", "balance");
		assertThat(response.code()).isEqualTo(OK.getCode());
	}

	@DisplayName("Should return 404 when account don't exits")
	@Test
	void shouldReturn404IfAccountDontExists() throws IOException {
		// When
		Request request = new Request.Builder()
				.url("http://localhost:8081/api/account/5")
				.get()
				.build();
		Response response = client.newCall(request).execute();

		// Then
		assertThat(response.code()).isEqualTo(NOT_FOUND.getCode());
	}

	@DisplayName("Should deposit money")
	@Test
	void shouldDepositMoney() throws IOException {
		// given
		database.addAccount(1L);

		Transfer transfer = new Transfer(0L, 1L, 100L);
		String jsonInString = mapper.writeValueAsString(transfer);

		// when
		RequestBody body = RequestBody.create(JSON, jsonInString);
		Request request = new Request.Builder()
				.url("http://localhost:8081/api/account/deposit")
				.post(body)
				.build();
		Response response = client.newCall(request).execute();

		// then
		assertThat(response.code()).isEqualTo(NO_CONTENT.getCode());
		assertThat(database.getAccount(1L).getBalance()).isEqualTo(100L);
	}

	@DisplayName("Should create account")
	@Test
	void shouldCreateAccount() throws IOException {
		// Given
		RequestBody body = RequestBody.create(null, new byte[0]);
		// When
		Request request = new Request.Builder()
				.url("http://localhost:8081/api/account/10/create")
				.post(body)
				.build();
		Response response = client.newCall(request).execute();

		// then
		assertThat(response.code()).isEqualTo(NO_CONTENT.getCode());
		assertThat(database.getAccount(10L)).isNotNull();
		assertThat(database.getAccount(10L).getBalance()).isEqualTo(0L);

		Request getRequest = new Request.Builder()
				.url("http://localhost:8081/api/account/10")
				.get()
				.build();
		Response getResponse = client.newCall(getRequest).execute();

		// Then
		String accountBalance = getResponse.body().string();
		Account account = mapper.readValue(accountBalance, Account.class);
		assertThat(account).isEqualToComparingOnlyGivenFields(new Account(10L, 0L), "uid", "balance");
		assertThat(getResponse.code()).isEqualTo(OK.getCode());
	}

}