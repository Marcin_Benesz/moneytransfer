package com.github.mbenesz.moneytransfer.transfer;


import com.github.mbenesz.moneytransfer.account.Account;
import com.github.mbenesz.moneytransfer.db.Database;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.Lock;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class TransferServiceTest {
	@DisplayName("Should transfer money from account 1 to account 2")
	@Test
	void shouldTransferMoneyFromAccount1toAccount() {
		// Given
		Database database = mock(Database.class);
		Account account1 = mock(Account.class);
		given(database.getAccount(1L)).willReturn(account1);
		Account account2 = mock(Account.class);
		given(database.getAccount(2L)).willReturn(account2);

		TransferService transferService = new TransferService(database);
		when(account1.getBalance()).thenReturn(100L);
		Lock lock = mock(Lock.class);
		when(account1.getLock()).thenReturn(lock);
		when(account2.getLock()).thenReturn(lock);
		when(lock.tryLock()).thenReturn(true);

		// When
		transferService.transfer(new Transfer(1, 2, 20));

		// Then
		verify(database, times(1)).getAccount(1L);
		verify(database, times(1)).withdraw(1L, 20L);
		verify(database, times(1)).deposit(2L, 20L);
	}

}