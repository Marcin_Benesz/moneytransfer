package com.github.mbenesz.moneytransfer.transfer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.mbenesz.moneytransfer.db.Database;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.eclipse.jetty.http.HttpStatus.Code.NO_CONTENT;
import static org.eclipse.jetty.http.HttpStatus.Code.UNPROCESSABLE_ENTITY;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
class MoneyTransferResourceIT {
	private Server server;
	private Database database;
	private OkHttpClient client = new OkHttpClient();
	private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	private ObjectMapper mapper = new ObjectMapper();

	@BeforeAll
	void before() throws Exception {
		database = Database.getInstance();

		server = new Server(8081);

		ServletContextHandler context = new ServletContextHandler();
		context.setContextPath("/");
		server.setHandler(context);

		ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/api/*");
		servletHolder.setInitOrder(0);
		servletHolder.setInitParameter(
				"jersey.config.server.provider.packages",
				"com.github.mbenesz.moneytransfer"
		);
		server.setHandler(context);
		server.start();
	}

	@AfterAll
	void after() {
		try {
			server.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterEach
	void afterEach() {
		database.clear();
	}

	@DisplayName("Should transfer money from one account to another")
	@Test
	void shouldTransferMoneyFromOneAccountToAnother() throws IOException {
		// given
		database.addAccount(1L);
		database.addAccount(2L);
		database.deposit(1L, 400L);
		database.deposit(2L, 300L);
		Transfer transfer = new Transfer(1L, 2L, 100L);
		String jsonInString = mapper.writeValueAsString(transfer);

		// when
		RequestBody body = RequestBody.create(JSON, jsonInString);
		Request request = new Request.Builder()
				.url("http://localhost:8081/api/transfer")
				.post(body)
				.build();
		Response response = client.newCall(request).execute();

		// then
		assertThat(response.code()).isEqualTo(NO_CONTENT.getCode());
		assertThat(database.getAccount(1L).getBalance()).isEqualTo(300L);
		assertThat(database.getAccount(2L).getBalance()).isEqualTo(400L);
	}

	@DisplayName("Should not transfer money from one account to another if there are no enough founds")
	@Test
	void shouldNotTransferMoneyFromOneAccountToAnotherIfThereAreNoEnoughFounds() throws IOException {
		// given
		database.addAccount(1L);
		database.addAccount(2L);
		database.deposit(1L, 400L);
		database.deposit(2L, 300L);
		Transfer transfer = new Transfer(1L, 2L, 500L);
		String jsonInString = mapper.writeValueAsString(transfer);

		// when
		RequestBody body = RequestBody.create(JSON, jsonInString);
		Request request = new Request.Builder()
				.url("http://localhost:8081/api/transfer")
				.post(body)
				.build();
		Response response = client.newCall(request).execute();

		// Then
		assertThat(response.code()).isEqualTo(UNPROCESSABLE_ENTITY.getCode());
		assertThat(database.getAccount(1L).getBalance()).isEqualTo(400L);
		assertThat(database.getAccount(2L).getBalance()).isEqualTo(300L);
	}
}