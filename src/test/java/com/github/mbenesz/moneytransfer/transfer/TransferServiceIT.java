package com.github.mbenesz.moneytransfer.transfer;

import com.github.mbenesz.moneytransfer.db.Database;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.parallel.Execution;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.junit.jupiter.api.parallel.ExecutionMode.CONCURRENT;

@Execution(CONCURRENT)
@TestInstance(PER_CLASS)
class TransferServiceIT {

	private Database database;

	@BeforeAll
	void setup() {
		database = Database.getInstance();
	}

	@BeforeEach
	void beforeEach(){
		database.addAccount(1L);
		database.addAccount(2L);
		database.addAccount(3L);
		database.deposit(1L, 200L);
	}

	@AfterEach
	void afterEach(){
		database.clear();
	}

	@DisplayName("Should not transfer money from account 1 to account 2 when insufficient fund")
	@Test
	void shouldNotTransferMoneyWhenInsufficientFund() {
		// Given
		Transfer transfer = new Transfer(1L, 2L, 1000L);
		TransferService transferService = new TransferService(database);

		// When
		assertThatThrownBy(() -> transferService.transfer(transfer))
				.isInstanceOf(InsufficientFundsException.class)
				.hasMessageContaining("Insufficient found");

		// Then
		assertThat(database.getAccount(1L).getBalance()).isEqualTo(200L);
		assertThat(database.getAccount(2L).getBalance()).isEqualTo(0L);
	}


	@DisplayName("Should transfer money from account 1 to account 2")
	@Test
	void shouldTransferMoneyFromAccount1toAccount2() throws InterruptedException {
		// Given
		Transfer transfer = new Transfer(1L, 2L, 1L);
		TransferService transferService = new TransferService(database);
		List<Thread> list = new ArrayList<>();

		// When
		for (int i = 0; i < 200; i++) {
			list.add(new Thread(() -> transferService.transfer(transfer)));

		}
		runConcurrent("Should transfer money from account 1 to account 2", list, 2000);
		// Then

		assertThat(database.getAccount(1L).getBalance()).isEqualTo(0L);
		assertThat(database.getAccount(2L).getBalance()).isEqualTo(200L);
	}

	static void runConcurrent(String message, List<? extends Runnable> runnables, int maxTimeoutSeconds)
			throws InterruptedException {
		int numThreads = runnables.size();
		List<Throwable> exceptions = Collections.synchronizedList(new ArrayList<>());
		ExecutorService threadPool = Executors.newFixedThreadPool(numThreads);
		try {
			CountDownLatch allExecutorThreadsReady = new CountDownLatch(numThreads);
			CountDownLatch afterInitBlocker = new CountDownLatch(1);
			CountDownLatch allDone = new CountDownLatch(numThreads);
			for (Runnable submittedTestRunnable : runnables) {
				threadPool.submit(() -> {
					allExecutorThreadsReady.countDown();
					try {
						afterInitBlocker.await();
						submittedTestRunnable.run();
					} catch (final Throwable e) {
						exceptions.add(e);
					} finally {
						allDone.countDown();
					}
				});
			}
			// wait until all threads are ready
			assertTrue(allExecutorThreadsReady.await(runnables.size() * 10, TimeUnit.MILLISECONDS), "Timeout initializing threads");
			// start all
			afterInitBlocker.countDown();
			assertTrue(allDone.await(maxTimeoutSeconds, TimeUnit.SECONDS), message + " timeout" + maxTimeoutSeconds + "seconds passed");
		} finally {
			threadPool.shutdownNow();
		}
		assertTrue(exceptions.isEmpty(), message + "failed with exception(s)" + exceptions);
	}
}