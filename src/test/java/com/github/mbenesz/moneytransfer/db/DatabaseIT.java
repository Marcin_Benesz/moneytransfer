package com.github.mbenesz.moneytransfer.db;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
class DatabaseIT {
	private Database database;

	@BeforeAll
	void setup() {
		database = Database.getInstance();
	}

	@BeforeEach
	void beforeEach() {
		database.addAccount(1L);
		database.addAccount(2L);
		database.addAccount(3L);
		database.deposit(1L, 200L);
	}

	@AfterEach
	void afterEach() {
		database.clear();
	}

	@Test
	@DisplayName("Should add account")
	void shouldAddAccount() {
		// Given
		long uid = 20;
		assertThatThrownBy(() -> database.getAccount(uid))
				.isInstanceOf(NoSuchAccountException.class)
				.hasMessageContaining("user " + uid + " not exists");

		// When
		database.addAccount(uid);

		// Then
		assertThat(database.getAccount(uid)).isNotNull();
	}

	@Test
	@DisplayName("Should withdraw money from account")
	void withdraw() {
		// When
		database.withdraw(1L, 2L);

		// Then
		assertThat(database.getAccount(1L).getBalance()).isEqualTo(198L);
	}

	@Test
	@DisplayName("Should deposit money to account")
	void deposit() {
		// When
		database.deposit(2L, 20L);

		// Then
		assertThat(database.getAccount(2L).getBalance()).isEqualTo(20L);
	}

	@Test
	@DisplayName("Should throw NoSuchAccountException when there is no account with provided uid")
	void shouldThrowExceptionWhenAccountDontExists() {
		// Given
		long uid = 199L;

		// When & Then
		assertThatThrownBy(() -> database.getAccount(uid))
				.isInstanceOf(NoSuchAccountException.class)
				.hasMessageContaining("user " + uid + " not exists");
	}

	@Test
	@DisplayName("Should clear database")
	void clear() {
		// Given not empty database
		long uid = 1L;
		long uid2 = 2L;
		assertThat(database.getAccount(uid)).isNotNull();
		assertThat(database.getAccount(uid2)).isNotNull();

		// when
		database.clear();

		// Then
		assertThatThrownBy(() -> database.getAccount(uid))
				.isInstanceOf(NoSuchAccountException.class)
				.hasMessageContaining("user " + uid + " not exists");

		assertThatThrownBy(() -> database.getAccount(uid2))
				.isInstanceOf(NoSuchAccountException.class)
				.hasMessageContaining("user " + uid2 + " not exists");
	}
}