package com.github.mbenesz.moneytransfer.db;


import com.github.mbenesz.moneytransfer.account.Account;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class Database {
	private Map<Long, Account> accounts = new ConcurrentHashMap<>();
	private static Database instance = new Database();

	private Database() {
	}

	public static Database getInstance() {
		return instance;
	}

	public void addAccount(long uid) {
		accounts.putIfAbsent(uid, new Account(uid, 0));
	}

	public long withdraw(Long uid, Long amount) {

		if (amount >= 0) {
			return Optional.ofNullable(accounts.get(uid))
					.orElseThrow(() -> new NoSuchElementException("user " + uid + " not exists"))
					.withdraw(amount);
		} else {
			throw new IllegalArgumentException("withdraw amount cannot be less or equal to zero");
		}

	}

	public long deposit(Long uid, Long amount) {
		if (amount >= 0) {
			return Optional.ofNullable(accounts.get(uid))
					.orElseThrow(() -> new NoSuchAccountException("user " + uid + " not exists"))
					.deposit(amount);
		} else {
			throw new IllegalArgumentException("deposit amount cannot be less or equal to zero");
		}
	}

	public Account getAccount(Long uid) {
		return Optional.ofNullable(accounts.get(uid))
				.orElseThrow(() -> new NoSuchAccountException("user " + uid + " not exists"));
	}

	public void clear() {
		accounts.clear();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}
}

