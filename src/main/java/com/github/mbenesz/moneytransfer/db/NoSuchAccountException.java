package com.github.mbenesz.moneytransfer.db;

public class NoSuchAccountException extends RuntimeException {
	public NoSuchAccountException(String s) {
		super(s);
	}
}
