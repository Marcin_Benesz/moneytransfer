package com.github.mbenesz.moneytransfer;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.eclipse.jetty.servlet.ServletContextHandler.NO_SESSIONS;

public class JerseyApplication {

	private static final Logger logger = LoggerFactory.getLogger(JerseyApplication.class);

	public static void main(String[] args) throws Exception {

		Server server = new Server(8080);

		ServletContextHandler servletContextHandler = new ServletContextHandler(NO_SESSIONS);

		servletContextHandler.setContextPath("/");
		server.setHandler(servletContextHandler);

		ServletHolder servletHolder = servletContextHandler.addServlet(ServletContainer.class, "/api/*");
		servletHolder.setInitParameter("com.sun.jersey.api.json.POJOMappingFeature", "true");
		servletHolder.setInitOrder(0);
		servletHolder.setInitParameter(
				"jersey.config.server.provider.packages",
				"com.github.mbenesz.moneytransfer"
		);

		try {
			server.start();
			server.join();
		} catch (Exception e){
			logger.error("error during server starting",e);
			server.stop();
			server.destroy();
			System.exit(1);
		}
	}
}
