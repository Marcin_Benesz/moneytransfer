package com.github.mbenesz.moneytransfer.transfer;

import com.github.mbenesz.moneytransfer.db.Database;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.NoSuchElementException;

import static org.eclipse.jetty.http.HttpStatus.Code.UNPROCESSABLE_ENTITY;

@Path("/transfer")
public class MoneyTransferResource {
	private Database database = Database.getInstance();
	private TransferService transferService = new TransferService(database);
	private static final Logger logger = LoggerFactory.getLogger(MoneyTransferResource.class);

	@POST
	@Produces(MediaType.TEXT_PLAIN)
	public Response transferMoney(Transfer transfer) {
		try {
			transferService.transfer(transfer);
			return Response.noContent().build();
		} catch (InsufficientFundsException e){
			logger.error("insufficient funds", e);
			return Response.noContent().status(UNPROCESSABLE_ENTITY.getCode()).build();
		} catch (NoSuchElementException e) {
			logger.error("missing account", e);
			return Response.noContent().status(Response.Status.NOT_FOUND).build();
		}
	}


}
