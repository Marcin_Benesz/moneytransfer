package com.github.mbenesz.moneytransfer.transfer;

public class InsufficientFundsException extends RuntimeException {
	public InsufficientFundsException(String message) {
		super(message);
	}
}
