package com.github.mbenesz.moneytransfer.transfer;
//@XmlRootElement
public class Transfer {
	private long fromUid;
	private long toUid;
	private long amount;

	public Transfer() {
	}

	public Transfer(long fromUid, long toUid, long amount) {
		this.fromUid = fromUid;
		this.toUid = toUid;
		this.amount = amount;
	}

	public long getFromUid() {
		return fromUid;
	}

	public long getToUid() {
		return toUid;
	}

	public long getAmount() {
		return amount;
	}

}
