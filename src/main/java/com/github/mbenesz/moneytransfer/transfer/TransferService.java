package com.github.mbenesz.moneytransfer.transfer;

import com.github.mbenesz.moneytransfer.account.Account;
import com.github.mbenesz.moneytransfer.db.Database;

import java.util.concurrent.TimeUnit;

class TransferService {
	private static final long TIMEOUT = 1L; // waiting time for "locked" account
	private static final TimeUnit UNIT = TimeUnit.SECONDS;
	private Database database;

	TransferService(Database database) {
		this.database = database;
	}

	public boolean transfer(Transfer transfer) throws InsufficientFundsException {

		Account fromAccount = database.getAccount(transfer.getFromUid());
		Account toAccount = database.getAccount(transfer.getToUid());

		long stopTime = System.nanoTime() + UNIT.toNanos(TIMEOUT);
		while (true) {
			if (fromAccount.getLock().tryLock()) {
				try {
					if (toAccount.getLock().tryLock()) {
						try {
							database.withdraw(transfer.getFromUid(), transfer.getAmount());
							database.deposit(transfer.getToUid(), transfer.getAmount());
							return true;
						} finally {
							toAccount.getLock().unlock();
						}
					}
				} finally {
					fromAccount.getLock().unlock();
				}
			}
			if (System.nanoTime() > stopTime)
				return false;
		}
	}
}

