package com.github.mbenesz.moneytransfer.account;

//import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.mbenesz.moneytransfer.transfer.InsufficientFundsException;

//import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
//@XmlRootElement
public class Account {
	private long uid;
	private AtomicLong balance = new AtomicLong();

	private transient Lock lock;

	public Account() {
		lock = new ReentrantLock();
	}

	public Account(long uid, long balance) {
		this.uid = uid;
		this.balance = new AtomicLong(balance);
		lock = new ReentrantLock();
	}

	public long getUid() {
		return uid;
	}

	public long getBalance() {
		return balance.get();
	}

	@JsonIgnore
	public Lock getLock() {
		return lock;
	}

	public long withdraw(long amount) {
		if (getBalance() < amount) {
			throw new InsufficientFundsException("Insufficient found");
		}
		return balance.addAndGet(-amount);
	}

	public long deposit(long amount) {
		return balance.addAndGet(amount);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Account account = (Account) o;
		return uid == account.uid &&
				balance.equals(account.balance);
	}

	@Override
	public int hashCode() {
		return Objects.hash(uid, balance);
	}
}