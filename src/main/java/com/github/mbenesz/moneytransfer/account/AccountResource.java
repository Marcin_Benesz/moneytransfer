package com.github.mbenesz.moneytransfer.account;

import com.github.mbenesz.moneytransfer.db.Database;
import com.github.mbenesz.moneytransfer.db.NoSuchAccountException;
import com.github.mbenesz.moneytransfer.transfer.Transfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/account")
public class AccountResource {
	private Database database = Database.getInstance();
	private static final Logger logger = LoggerFactory.getLogger(Database.class);

	@GET
	@Path("/{uid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Account getAccount(@PathParam("uid") long uid) {
		try {
			Account account = database.getAccount(uid);
			return new Account(account.getUid(), account.getBalance());
		} catch (NoSuchAccountException e) {
			logger.error("account don't exists", e);
			throw new WebApplicationException(404);
		}
	}


//	@GET
//	@Path("/{uid}")
//	@Produces(MediaType.TEXT_PLAIN)
//	public long getBalance(@PathParam("uid") long uid) {
//				try {
//		Account account = database.getAccount(uid);
//		return account.getBalance();
//	} catch (NoSuchAccountException e) {
//			logger.error("account don't exists", e);
//			throw new WebApplicationException(404);
//		}
//	}

	@POST
	@Path("/{uid}/create")
	@Produces(MediaType.TEXT_PLAIN)
	public Response createAccount(@PathParam("uid") long uid) {
		database.addAccount(uid);
		return Response.noContent().build();
	}

	@POST
	@Path("/deposit")
	@Consumes("application/json")
	public Response depositMoney(Transfer transfer) {
		database.deposit(transfer.getToUid(), transfer.getAmount());
		return Response.noContent().build();
	}
}
